#!/bin/bash
# This is a general installation script for the photon-airflow project.  It's
# a bit hackey, but it should work consistently if you don't change things.

###############################
# Globals                     #
###############################
MY_USER=$USER


###############################
# Prep                        #
###############################
echo "Updating and removing old docker installations if they exist."
sudo apt-get update || exit 1
sudo apt-get remove docker docker-engine docker.io || exit 1
sudo apt-get install -y software-properties-common || exit 1


###############################
# Keys                        #
###############################
echo "Adding GPG keys"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - || exit 1


###############################
# Repositories                #
###############################
echo "Adding repositories"
sudo add-apt-repository -y ppa:deadsnakes/ppa || exit 1
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable" || exit 1


###############################
# Services                    #
###############################
echo "Updating and installing required services."
sudo apt-get update || exit 1
sudo apt install -y python3.6 \
                    docker-ce || exit 1


###############################
# Users/Groups                #
###############################
echo "Adding docker group and adding $MY_USER to group."
sudo groupadd docker
sudo usermod -aG docker $MY_USER


###############################
# Python Packages             #
###############################
echo "Getting pip for python3.6"
curl https://bootstrap.pypa.io/get-pip.py | sudo python3.6 || exit 1

# Packages needed for other stuff.
echo "Installing docker-compose"
sudo -H python3.6 -m pip install --upgrade docker-compose || exit 1


# initialize as swarm master
echo "Initializing docker swarm"
sudo docker swarm init || exit 1

echo "Setup complete - log out and back in for permissions to take effect."
