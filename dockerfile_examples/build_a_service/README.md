# Building a service in docker

Let's say I need a database, and I want to use a docker container for that.  I also want this database to have volumes that don't disappear if I make changes on it.  Here's how we'll do it.

In my terminal, I'll run a "detached" container from the "mariadb" image and give it access to port 3306 of the host it's running on.  I'll also give it a root password.
`docker run -d -p 3306:3306 --name mariadb -e MYSQL_ROOT_PASSWORD=root mariadb`

Now from my host, I should be able to run the mysql command and get to the mysql console!
`mysql -h 0.0.0.0 -u root -proot`
