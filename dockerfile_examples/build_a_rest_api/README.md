# Building a rest API in a docker container

Build the container
`docker build -t 'flask_rest_api' .`

Run the container in "detached" mode - meaning let it keep running without you
`docker run -d -p 5002:5002 flask_rest_api`

Navigate to <ipaddr>:5001 and you'll see your index.
