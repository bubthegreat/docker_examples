from flask import Flask
import datetime

app = Flask(__name__)

@app.route('/')
def index():
    return "You got to my index!"

@app.route('/api/time/')
def time():
    return str(datetime.datetime.now())

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5002)
