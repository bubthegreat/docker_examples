# How to not break your host while experimenting!

Sometimes you might want to test something like an installation and see if it does what you want, but you don't want to have to uninstall it all if it doesn't work out.  Docker is great for that.

`docker run -it --name 'test_ubuntu_installs' ubuntu:xenial`

Now you can do whatever you want, and it's isolated to that docker image.

Try running `rm -rf /` and see what happens - then exit the container and verify that you didn't ruin your day!
