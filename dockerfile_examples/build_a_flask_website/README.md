# Building a flask website in a docker container
Build the container
`docker build -t 'flask_website' .`

Run the container in "detached" mode - meaning let it keep running without you
`docker run -d -p 5001:5001 flask_website`

Navigate to <ipaddr>:5001 and you'll see your index.
