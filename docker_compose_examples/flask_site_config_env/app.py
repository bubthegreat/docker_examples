# compose_flask/app.py

import os

from flask import Flask
from redis import Redis

DEV_NAME = os.environ.get('DEV_NAME') or 'Not Sure'
APP_PORT = os.environ.get('APP_PORT') or 5000

app = Flask(__name__)
redis = Redis(host='redis', port=6379)

@app.route('/')
def hello():
    redis.incr('hits')
    return f"This demo site by {DEV_NAME} has been viewed {redis.get('hits')} time(s)."


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True, port=APP_PORT)
