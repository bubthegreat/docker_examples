# compose_flask/app.py

import os

from flask import Flask
from redis import Redis

REDIS_PORT = os.environ.get('REDIS_PORT') or 6379
APP_PORT = os.environ.get('APP_PORT') or 5000

app = Flask(__name__)
redis = Redis(host='redis', port=6379)

@app.route('/')
def hello():
    redis.incr('hits')
    return 'This Compose/Flask demo has been viewed %s time(s).' % redis.get('hits')


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True, port=APP_PORT)
